import Vue from 'vue';
import HelloCmp from './hello.vue';
import router from './router';
import store from './store'

const v = new Vue({
    router,
    store,
    el: '#app',
    template: `
    <div>
        Name: <input v-model="name" type="text">
        <hello-cmp :name="name" :initialEnthusiasm="5" />
        <div class="greeting">Tomáš</div>
        <router-view></router-view>
        <router-link to="/detail">Detail</router-link>
    </div>
`,
    data: { name: 'EEE' },
    components: {
        HelloCmp
    }
});
