import Vue from 'vue';
import VueRouter from 'vue-router';

import IndexPage from './index-page.vue';
import DetailPage from './detail-page.vue';

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'index',
            component: IndexPage
        },
        {
            path: '/detail',
            name: 'detail',
            component: DetailPage
        }
    ]
})
