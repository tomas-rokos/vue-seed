import Vue from 'vue'
import Vuex from 'vuex'
import { MutationTree, ActionTree } from 'vuex'

Vue.use(Vuex)

interface State {
    links: { url: string, description: string }[];
};

const mutations: MutationTree<State> = {
    add: (st, ctx) => {
        st.links.push({ url: ctx.a, description: 'xx' });
    },
    remove: (st, ctx) => {
        st.links.push({ url: ctx.a, description: 'xx' });
    }
};

const actions: ActionTree<State, any> = {
    addAction({ commit }): any {
        commit('add', { 'x': 'y' });
    }
};

const state: State = {
    links: [
        { url: 'https://vuejs.org', description: 'Core Docs' },
        { url: 'https://forum.vuejs.org', description: 'Forum' },
        { url: 'https://chat.vuejs.org', description: 'Community Chat' }
    ]
};

const moduleA = {
    state,
    mutations,
    actions
};

export default new Vuex.Store({
    modules: {
        a: moduleA
    }
});
